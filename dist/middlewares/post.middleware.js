"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.postExists = void 0;
const HttpEnums_1 = require("../helpers/HttpEnums");
const post_service_1 = __importDefault(require("../post/post.service"));
function postExists(req, res, next) {
    return __awaiter(this, void 0, void 0, function* () {
        try {
            let post_id = Number(req.params.post_id);
            let does_exists = yield post_service_1.default.findOne(post_id, next);
            if (!does_exists.id) {
                res.status(HttpEnums_1.ErrorStatus.BAD_REQUEST).json({
                    error: true,
                    msg: HttpEnums_1.ErrorMsg.BAD_REQUEST,
                });
                return;
            }
            next();
        }
        catch (error) {
            return next(error);
        }
    });
}
exports.postExists = postExists;
//# sourceMappingURL=post.middleware.js.map