"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.authUser = void 0;
const HttpEnums_1 = require("../helpers/HttpEnums");
const jwt_1 = require("../helpers/jwt");
function authUser(req, res, next) {
    try {
        let access_token = req.headers["token"];
        if (!access_token || !access_token.length) {
            res.status(HttpEnums_1.ErrorStatus.NOT_AUTH).json({
                error: true,
                msg: HttpEnums_1.ErrorMsg.NOT_AUTH,
            });
            return;
        }
        let decoded_jwt = (0, jwt_1.decodeJwt)(access_token);
        if (!decoded_jwt || !decoded_jwt.data.id) {
            res.status(HttpEnums_1.ErrorStatus.NOT_AUTH).json({
                error: true,
                msg: HttpEnums_1.ErrorMsg.NOT_AUTH,
            });
            return;
        }
        req.user = decoded_jwt.data;
        next();
    }
    catch (error) {
        return next(error);
    }
}
exports.authUser = authUser;
//# sourceMappingURL=auth.middleware.js.map