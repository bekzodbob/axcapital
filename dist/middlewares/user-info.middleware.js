"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.userInfo = void 0;
const jwt_1 = require("../helpers/jwt");
function userInfo(req, res, next) {
    try {
        let refresh_cookie = req.headers.cookie;
        if (!refresh_cookie) {
            return next();
        }
        let refresh_token = refresh_cookie.split("=")[1];
        let decoded_jwt = (0, jwt_1.decodeJwt)(refresh_token);
        if (!decoded_jwt) {
            return next();
        }
        req.user = decoded_jwt.data;
        next();
    }
    catch (error) {
        return next(error);
    }
}
exports.userInfo = userInfo;
//# sourceMappingURL=user-info.middleware.js.map