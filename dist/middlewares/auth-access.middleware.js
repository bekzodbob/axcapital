"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.authAccessUser = void 0;
const HttpEnums_1 = require("../helpers/HttpEnums");
const jwt_1 = require("../helpers/jwt");
const auth_service_1 = __importDefault(require("../auth/auth.service"));
function authAccessUser(req, res, next) {
    return __awaiter(this, void 0, void 0, function* () {
        let refresh_cookie = req.headers.cookie;
        if (!refresh_cookie) {
            res.status(HttpEnums_1.ErrorStatus.NOT_AUTH).json({
                error: true,
                msg: HttpEnums_1.ErrorMsg.NOT_AUTH,
            });
            return;
        }
        let refresh_token = refresh_cookie.split("=")[1];
        let decoded_jwt = (0, jwt_1.decodeJwt)(refresh_token);
        if (!decoded_jwt) {
            res.status(HttpEnums_1.ErrorStatus.NOT_AUTH).json({
                error: true,
                msg: HttpEnums_1.ErrorMsg.NOT_AUTH,
            });
            return;
        }
        let refresh_active = yield auth_service_1.default.findRefresh(decoded_jwt.data.id, refresh_token, next);
        if (!refresh_active) {
            res.status(HttpEnums_1.ErrorStatus.NOT_AUTH).json({
                error: true,
                msg: HttpEnums_1.ErrorMsg.NOT_AUTH,
            });
            return;
        }
        req.user = decoded_jwt.data;
        next();
    });
}
exports.authAccessUser = authAccessUser;
//# sourceMappingURL=auth-access.middleware.js.map