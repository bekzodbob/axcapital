"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const post_service_1 = __importDefault(require("../post/post.service"));
const HttpEnums_1 = require("../helpers/HttpEnums");
const user_service_1 = __importDefault(require("./user.service"));
class UserController {
    getAllUser(req, res, next) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                let offset = Number(req.query.last_id) || 0;
                let result = yield user_service_1.default.getAllUsers(offset, next);
                res.status(HttpEnums_1.SuccessStatus.OK).json({
                    error: null,
                    msg: HttpEnums_1.SuccessMsg.OK,
                    result,
                });
            }
            catch (error) {
                next(error);
            }
        });
    }
    getUser(req, res, next) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                let user_id = Number(req.params.user_id);
                let result = yield user_service_1.default.getUser(user_id, next);
                res.status(HttpEnums_1.SuccessStatus.OK).json({
                    error: null,
                    msg: HttpEnums_1.SuccessMsg.OK,
                    result,
                });
            }
            catch (error) {
                next(error);
            }
        });
    }
    getUserPosts(req, res, next) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                let user_id = Number(req.params.user_id);
                let last_id = Number(req.query.last_id) || 0;
                let result = yield post_service_1.default.getUserPosts(user_id, last_id, next);
                res.status(HttpEnums_1.SuccessStatus.OK).json({
                    error: null,
                    msg: HttpEnums_1.SuccessMsg.OK,
                    result,
                });
            }
            catch (error) {
                next(error);
            }
        });
    }
}
exports.default = new UserController();
//# sourceMappingURL=user.controller.js.map