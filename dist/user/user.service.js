"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const User_model_1 = require("./entities/User.model");
const sequelize_1 = __importDefault(require("sequelize"));
class UserService {
    getAllUsers(offset, next) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                if (offset) {
                    let result = yield User_model_1.User.findAll({
                        where: {
                            id: {
                                [sequelize_1.default.Op.gt]: offset,
                            },
                        },
                        order: [["id", "ASC"]],
                        limit: 10,
                        attributes: ["id", "email", "created_at"],
                    });
                    return result;
                }
                else {
                    let result = yield User_model_1.User.findAll({
                        order: [["id", "ASC"]],
                        limit: 10,
                        attributes: ["id", "email", "created_at"],
                    });
                    return result;
                }
            }
            catch (error) {
                next(error);
            }
        });
    }
    getUser(id, next) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                let result = yield User_model_1.User.findOne({
                    where: {
                        id,
                    },
                    attributes: ["id", "email", "created_at"],
                });
                return result;
            }
            catch (error) {
                next(error);
            }
        });
    }
    findOne(email, next) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                let result = yield User_model_1.User.findOne({
                    where: {
                        email,
                    },
                    include: [{ all: true }],
                });
                return result;
            }
            catch (error) {
                next(error);
            }
        });
    }
}
exports.default = new UserService();
//# sourceMappingURL=user.service.js.map