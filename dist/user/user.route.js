"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = require("express");
const user_controller_1 = __importDefault(require("./user.controller"));
const router = (0, express_1.Router)();
router.get("/", user_controller_1.default.getAllUser);
router.get("/:user_id", user_controller_1.default.getUser);
router.get("/:user_id/post", user_controller_1.default.getUserPosts);
exports.default = router;
//# sourceMappingURL=user.route.js.map