"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.err = exports.log = void 0;
function log(msg) {
    console.log(`${new Date()}: ${msg}`);
}
exports.log = log;
function err(msg) {
    console.log(`${new Date()}: ${msg}`);
}
exports.err = err;
//# sourceMappingURL=logger.service.js.map