"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.notFoundHandler = void 0;
const HttpEnums_1 = require("./HttpEnums");
function notFoundHandler(req, res, next) {
    res.status(HttpEnums_1.ErrorStatus.NOT_FOUND).json({
        error: true,
        msg: HttpEnums_1.ErrorMsg.NOT_FOUND,
    });
    return;
}
exports.notFoundHandler = notFoundHandler;
//# sourceMappingURL=not-found.handler.js.map