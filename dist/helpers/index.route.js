"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = require("express");
const auth_route_1 = __importDefault(require("../auth/auth.route"));
const post_route_1 = __importDefault(require("../post/post.route"));
const user_route_1 = __importDefault(require("../user/user.route"));
const router = (0, express_1.Router)();
router.use("/auth", auth_route_1.default);
router.use("/post", post_route_1.default);
router.use("/user", user_route_1.default);
exports.default = router;
//# sourceMappingURL=index.route.js.map