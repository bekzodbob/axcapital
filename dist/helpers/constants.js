"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.REFRESH_TOKEN_AGE = exports.ACCESS_TOKEN_AGE = void 0;
const ACCESS_TOKEN_AGE = () => {
    return Math.floor(Date.now() / 1000) + 60 * 5;
};
exports.ACCESS_TOKEN_AGE = ACCESS_TOKEN_AGE;
const REFRESH_TOKEN_AGE = () => {
    return Math.floor(Date.now() / 1000) + 60 * 60 * 24 * 30;
};
exports.REFRESH_TOKEN_AGE = REFRESH_TOKEN_AGE;
//# sourceMappingURL=constants.js.map