"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const sequelize_1 = require("sequelize");
const Post_Rating_1 = require("../post/entities/Post-Rating");
function calculateAverage(post_id, next) {
    return __awaiter(this, void 0, void 0, function* () {
        try {
            const result = yield Post_Rating_1.PostRating.findOne({
                attributes: [
                    [sequelize_1.Sequelize.fn("AVG", sequelize_1.Sequelize.col("value")), "averageRating"],
                ],
                where: { post_id },
            });
            let average = result.get("averageRating");
            return average;
        }
        catch (error) {
            next(error);
        }
    });
}
exports.default = calculateAverage;
//# sourceMappingURL=calculate-average.js.map