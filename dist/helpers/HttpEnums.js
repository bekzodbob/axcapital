"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.SuccessStatus = exports.SuccessMsg = exports.ErrorStatus = exports.ErrorMsg = void 0;
var ErrorMsg;
(function (ErrorMsg) {
    ErrorMsg["SERVER_ERROR"] = "SERVER_ERROR";
    ErrorMsg["WRONG_CREDENTIALS"] = "WRONG_CREDENTIALS";
    ErrorMsg["NOT_AUTH"] = "NOT_AUTH";
    ErrorMsg["NOT_FOUND"] = "NOT_FOUND";
    ErrorMsg["ALREADY_EXISTS"] = "ALREADY_EXISTS";
    ErrorMsg["BAD_REQUEST"] = "BAD_REQUEST";
    ErrorMsg["INVALID_EMAIL"] = "INVALID_EMAIL";
    ErrorMsg["INVALID_PASSWORD"] = "INVALID_PASSWORD";
    ErrorMsg["INVALID_TITLE"] = "INVALID_TITLE";
    ErrorMsg["INVALID_CONTENT"] = "INVALID_CONTENT";
    ErrorMsg["INVALID_RATING"] = "INVALID_RATING";
})(ErrorMsg = exports.ErrorMsg || (exports.ErrorMsg = {}));
var ErrorStatus;
(function (ErrorStatus) {
    ErrorStatus[ErrorStatus["SERVER_ERROR"] = 503] = "SERVER_ERROR";
    ErrorStatus[ErrorStatus["WRONG_CREDENTIALS"] = 400] = "WRONG_CREDENTIALS";
    ErrorStatus[ErrorStatus["NOT_AUTH"] = 401] = "NOT_AUTH";
    ErrorStatus[ErrorStatus["NOT_FOUND"] = 404] = "NOT_FOUND";
    ErrorStatus[ErrorStatus["BAD_REQUEST"] = 400] = "BAD_REQUEST";
})(ErrorStatus = exports.ErrorStatus || (exports.ErrorStatus = {}));
var SuccessMsg;
(function (SuccessMsg) {
    SuccessMsg["OK"] = "OK";
    SuccessMsg["CREATED"] = "CREATED";
})(SuccessMsg = exports.SuccessMsg || (exports.SuccessMsg = {}));
var SuccessStatus;
(function (SuccessStatus) {
    SuccessStatus[SuccessStatus["OK"] = 200] = "OK";
    SuccessStatus[SuccessStatus["CREATED"] = 201] = "CREATED";
})(SuccessStatus = exports.SuccessStatus || (exports.SuccessStatus = {}));
//# sourceMappingURL=HttpEnums.js.map