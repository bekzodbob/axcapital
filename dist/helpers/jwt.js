"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.decodeJwt = exports.signJwt = void 0;
const jsonwebtoken_1 = __importDefault(require("jsonwebtoken"));
const constants_1 = require("./constants");
const logger_service_1 = require("./logger.service");
const SECRET_KEY = String(process.env.JWT_SERVER_KEY);
function signJwt(user) {
    try {
        let access_token = jsonwebtoken_1.default.sign({
            exp: (0, constants_1.ACCESS_TOKEN_AGE)(),
            data: user,
        }, SECRET_KEY);
        let refresh_token = jsonwebtoken_1.default.sign({
            exp: (0, constants_1.REFRESH_TOKEN_AGE)(),
            data: user,
        }, SECRET_KEY);
        return { refresh_token, access_token };
    }
    catch (error) {
        (0, logger_service_1.err)(error);
    }
}
exports.signJwt = signJwt;
function decodeJwt(token) {
    try {
        let decoded = jsonwebtoken_1.default.verify(token, SECRET_KEY);
        return decoded;
    }
    catch (error) {
        (0, logger_service_1.err)(error);
    }
}
exports.decodeJwt = decodeJwt;
//# sourceMappingURL=jwt.js.map