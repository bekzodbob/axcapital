"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.errorHandler = void 0;
const HttpEnums_1 = require("./HttpEnums");
const logger_service_1 = require("./logger.service");
function errorHandler(error, req, res, next) {
    (0, logger_service_1.err)(error);
    res.status(HttpEnums_1.ErrorStatus.SERVER_ERROR).json({
        error: true,
        msg: HttpEnums_1.ErrorMsg.SERVER_ERROR,
    });
    return;
}
exports.errorHandler = errorHandler;
//# sourceMappingURL=error.handler.js.map