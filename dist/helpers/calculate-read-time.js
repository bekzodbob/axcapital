"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
function readTime(content) {
    return Math.round(content.trim().split(/\s+/).length / 238);
}
exports.default = readTime;
//# sourceMappingURL=calculate-read-time.js.map