"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = require("express");
const express_validator_1 = require("express-validator");
const HttpEnums_1 = require("../helpers/HttpEnums");
const auth_middleware_1 = require("../middlewares/auth.middleware");
const user_info_middleware_1 = require("../middlewares/user-info.middleware");
const post_controller_1 = __importDefault(require("./post.controller"));
const router = (0, express_1.Router)();
router.post("/", (0, express_validator_1.check)("title")
    .isLength({ min: 10, max: 150 })
    .withMessage(HttpEnums_1.ErrorMsg.INVALID_TITLE)
    .bail(), (0, express_validator_1.check)("content")
    .isLength({ min: 10, max: 25000 })
    .withMessage(HttpEnums_1.ErrorMsg.INVALID_CONTENT)
    .bail(), auth_middleware_1.authUser, post_controller_1.default.create);
router.get("/", post_controller_1.default.getAll);
router.get("/:id", user_info_middleware_1.userInfo, post_controller_1.default.getOne);
router.post("/:id/rate", (0, express_validator_1.check)("value")
    .isInt({ min: 1, max: 5 })
    .withMessage(HttpEnums_1.ErrorMsg.INVALID_RATING)
    .bail(), auth_middleware_1.authUser, post_controller_1.default.ratePost);
router.put("/:id", (0, express_validator_1.check)("title")
    .isLength({ min: 10, max: 150 })
    .withMessage(HttpEnums_1.ErrorMsg.INVALID_TITLE)
    .bail(), (0, express_validator_1.check)("content")
    .isLength({ min: 10, max: 25000 })
    .withMessage(HttpEnums_1.ErrorMsg.INVALID_CONTENT)
    .bail(), auth_middleware_1.authUser, post_controller_1.default.update);
router.delete("/:id", auth_middleware_1.authUser, post_controller_1.default.delete);
exports.default = router;
//# sourceMappingURL=post.route.js.map