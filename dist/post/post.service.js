"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const User_model_1 = require("../user/entities/User.model");
const Post_model_1 = require("./entities/Post.model");
const sequelize_1 = __importDefault(require("sequelize"));
const Post_View_model_1 = require("./entities/Post-View.model");
const Post_Rating_1 = require("./entities/Post-Rating");
const calculate_average_1 = __importDefault(require("../helpers/calculate-average"));
const calculate_read_time_1 = __importDefault(require("../helpers/calculate-read-time"));
class PostService {
    create(user_id, title, content, next) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                let result = yield Post_model_1.Post.create({
                    author: user_id,
                    title,
                    content,
                });
                return result;
            }
            catch (error) {
                next(error);
            }
        });
    }
    getAll(offset, next) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                let result;
                if (offset) {
                    result = yield Post_model_1.Post.findAll({
                        where: {
                            id: {
                                [sequelize_1.default.Op.lt]: offset,
                            },
                        },
                        order: [["id", "DESC"]],
                        limit: 10,
                        include: [
                            {
                                model: User_model_1.User,
                                attributes: ["id", "email", "created_at"],
                            },
                        ],
                    });
                }
                else {
                    result = yield Post_model_1.Post.findAll({
                        order: [["id", "DESC"]],
                        limit: 10,
                        include: [
                            {
                                model: User_model_1.User,
                                attributes: ["id", "email", "created_at"],
                            },
                        ],
                    });
                }
                return result;
            }
            catch (error) {
                next(error);
            }
        });
    }
    getUserPosts(user_id, offset, next) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                let result;
                if (offset) {
                    result = yield Post_model_1.Post.findAll({
                        where: {
                            id: {
                                [sequelize_1.default.Op.gt]: offset,
                            },
                            author: user_id,
                        },
                        order: [["id", "DESC"]],
                        limit: 10,
                    });
                }
                else {
                    result = yield Post_model_1.Post.findAll({
                        where: {
                            author: user_id,
                        },
                        order: [["id", "DESC"]],
                        limit: 10,
                    });
                }
                return result;
            }
            catch (error) {
                next(error);
            }
        });
    }
    getOne(id, next) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                let result = yield Post_model_1.Post.findOne({
                    where: {
                        id,
                    },
                    include: [
                        {
                            model: User_model_1.User,
                            attributes: ["id", "email", "created_at"],
                        },
                    ],
                });
                let { title, content, rating, author, created_at, updated_at, author_info, } = result;
                let read_time = (0, calculate_read_time_1.default)(content);
                return {
                    id,
                    title,
                    content,
                    rating,
                    author,
                    created_at,
                    updated_at,
                    author_info,
                    read_time,
                };
            }
            catch (error) {
                next(error);
            }
        });
    }
    update(post_id, user_id, title, content, next) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                let updated = yield Post_model_1.Post.update({
                    title,
                    content,
                }, {
                    where: {
                        id: post_id,
                        author: user_id,
                    },
                });
                if (!updated) {
                    return [];
                }
                let result = yield Post_model_1.Post.findOne({
                    where: {
                        id: post_id,
                        author: user_id,
                    },
                    include: [
                        {
                            model: User_model_1.User,
                            attributes: ["id", "email", "created_at"],
                        },
                    ],
                });
                return result;
            }
            catch (error) {
                next(error);
            }
        });
    }
    delete(post_id, user_id, next) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                let result = yield Post_model_1.Post.destroy({
                    where: {
                        id: post_id,
                        author: user_id,
                    },
                });
                return result;
            }
            catch (error) {
                next(error);
            }
        });
    }
    findOne(id, next) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                let result = yield Post_model_1.Post.findOne({
                    where: {
                        id,
                    },
                });
                return result;
            }
            catch (error) {
                next(error);
            }
        });
    }
    postView(post_id, user_id, next) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                let result = yield Post_View_model_1.PostViews.create({
                    post_id,
                    user_id,
                }, {
                    returning: true,
                });
                return result;
            }
            catch (error) {
                next(error);
            }
        });
    }
    ratePost(post_id, user_id, value, next) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                let result;
                let rated = yield Post_Rating_1.PostRating.findOne({
                    where: {
                        post_id,
                        user_id,
                    },
                });
                if (!rated) {
                    result = yield Post_Rating_1.PostRating.create({
                        post_id,
                        user_id,
                        value,
                    }, {
                        returning: true,
                    });
                }
                else {
                    yield Post_Rating_1.PostRating.update({
                        value,
                    }, {
                        where: {
                            post_id,
                            user_id,
                        },
                    });
                }
                result = yield Post_Rating_1.PostRating.findOne({
                    where: {
                        post_id,
                        user_id,
                    },
                });
                let post = yield this.findOne(post_id, next);
                let average_rating = yield (0, calculate_average_1.default)(post_id, next);
                yield Post_model_1.Post.update({
                    rating: average_rating,
                }, {
                    where: {
                        id: post_id,
                    },
                });
                yield User_model_1.User.update({
                    rating: average_rating,
                }, {
                    where: {
                        id: post.author,
                    },
                });
                return result;
            }
            catch (error) {
                next(error);
            }
        });
    }
}
exports.default = new PostService();
//# sourceMappingURL=post.service.js.map