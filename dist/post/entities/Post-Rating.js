"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.PostRating = void 0;
const sequelize_typescript_1 = require("sequelize-typescript");
const User_model_1 = require("../../user/entities/User.model");
const Post_model_1 = require("./Post.model");
let PostRating = class PostRating extends sequelize_typescript_1.Model {
};
__decorate([
    (0, sequelize_typescript_1.Column)({
        type: sequelize_typescript_1.DataType.BIGINT,
        unique: true,
        autoIncrement: true,
        primaryKey: true,
    })
], PostRating.prototype, "id", void 0);
__decorate([
    (0, sequelize_typescript_1.Column)({
        type: sequelize_typescript_1.DataType.SMALLINT,
        allowNull: false,
        validate: {
            min: 1,
            max: 5,
        },
    })
], PostRating.prototype, "value", void 0);
__decorate([
    (0, sequelize_typescript_1.ForeignKey)(() => User_model_1.User),
    (0, sequelize_typescript_1.Column)(sequelize_typescript_1.DataType.INTEGER)
], PostRating.prototype, "user_id", void 0);
__decorate([
    (0, sequelize_typescript_1.ForeignKey)(() => Post_model_1.Post),
    (0, sequelize_typescript_1.Column)(sequelize_typescript_1.DataType.INTEGER)
], PostRating.prototype, "post_id", void 0);
__decorate([
    sequelize_typescript_1.CreatedAt,
    (0, sequelize_typescript_1.Column)({ field: "created_at", type: sequelize_typescript_1.DataType.DATE })
], PostRating.prototype, "created_at", void 0);
__decorate([
    sequelize_typescript_1.UpdatedAt,
    (0, sequelize_typescript_1.Column)({ field: "updated_at", type: sequelize_typescript_1.DataType.DATE })
], PostRating.prototype, "updated_at", void 0);
PostRating = __decorate([
    (0, sequelize_typescript_1.Table)({ tableName: "post_ratings" })
], PostRating);
exports.PostRating = PostRating;
//# sourceMappingURL=Post-Rating.js.map