"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.PostViews = void 0;
const sequelize_typescript_1 = require("sequelize-typescript");
const User_model_1 = require("../../user/entities/User.model");
const Post_model_1 = require("./Post.model");
let PostViews = class PostViews extends sequelize_typescript_1.Model {
};
__decorate([
    (0, sequelize_typescript_1.Column)({
        type: sequelize_typescript_1.DataType.BIGINT,
        unique: true,
        autoIncrement: true,
        primaryKey: true,
    })
], PostViews.prototype, "id", void 0);
__decorate([
    (0, sequelize_typescript_1.ForeignKey)(() => User_model_1.User),
    (0, sequelize_typescript_1.Column)(sequelize_typescript_1.DataType.INTEGER)
], PostViews.prototype, "user_id", void 0);
__decorate([
    (0, sequelize_typescript_1.ForeignKey)(() => Post_model_1.Post),
    (0, sequelize_typescript_1.Column)(sequelize_typescript_1.DataType.INTEGER)
], PostViews.prototype, "post_id", void 0);
__decorate([
    sequelize_typescript_1.CreatedAt,
    (0, sequelize_typescript_1.Column)({ field: "created_at", type: sequelize_typescript_1.DataType.DATE })
], PostViews.prototype, "created_at", void 0);
PostViews = __decorate([
    (0, sequelize_typescript_1.Table)({ tableName: "post_views" })
], PostViews);
exports.PostViews = PostViews;
//# sourceMappingURL=Post-View.model.js.map