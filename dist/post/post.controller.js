"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const post_service_1 = __importDefault(require("./post.service"));
const HttpEnums_1 = require("../helpers/HttpEnums");
const express_validator_1 = require("express-validator");
class PostController {
    create(req, res, next) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                let { id } = req.user;
                let { title, content } = req.body;
                const { errors } = (0, express_validator_1.validationResult)(req);
                if (errors.length) {
                    return res.status(HttpEnums_1.ErrorStatus.BAD_REQUEST).json({
                        error: true,
                        msg: errors[0].msg,
                    });
                }
                let result = yield post_service_1.default.create(id, title, content, next);
                res.status(HttpEnums_1.SuccessStatus.CREATED).json({
                    error: null,
                    msg: HttpEnums_1.SuccessMsg.CREATED,
                    result,
                });
            }
            catch (error) {
                next(error);
            }
        });
    }
    getAll(req, res, next) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                let last_id = Number(req.query.last_id) || 0;
                let result = yield post_service_1.default.getAll(last_id, next);
                res.status(HttpEnums_1.SuccessStatus.OK).json({
                    error: null,
                    msg: HttpEnums_1.SuccessMsg.OK,
                    result,
                });
            }
            catch (error) {
                next(error);
            }
        });
    }
    getOne(req, res, next) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                let user_id = req.user.id || null;
                let id = Number(req.params.id);
                if (!id) {
                    res.status(HttpEnums_1.ErrorStatus.BAD_REQUEST).json({
                        error: true,
                        msg: HttpEnums_1.ErrorMsg.BAD_REQUEST,
                    });
                    return;
                }
                let post = yield post_service_1.default.getOne(id, next);
                if (!post) {
                    res.status(HttpEnums_1.ErrorStatus.NOT_FOUND).json({
                        error: true,
                        msg: HttpEnums_1.ErrorMsg.NOT_FOUND,
                        post,
                    });
                    return;
                }
                yield post_service_1.default.postView(id, user_id, next);
                res.status(HttpEnums_1.SuccessStatus.OK).json({
                    error: null,
                    msg: HttpEnums_1.SuccessMsg.OK,
                    post,
                });
            }
            catch (error) {
                next(error);
            }
        });
    }
    update(req, res, next) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                let { id: user_id } = req.user;
                let post_id = Number(req.params.id);
                let { title, content } = req.body;
                const { errors } = (0, express_validator_1.validationResult)(req);
                if (errors.length) {
                    return res.status(HttpEnums_1.ErrorStatus.BAD_REQUEST).json({
                        error: true,
                        msg: errors[0].msg,
                    });
                }
                let result = yield post_service_1.default.update(post_id, user_id, title, content, next);
                if (!result) {
                    res.status(HttpEnums_1.ErrorStatus.NOT_FOUND).json({
                        error: true,
                        msg: HttpEnums_1.ErrorMsg.NOT_FOUND,
                    });
                    return;
                }
                res.status(HttpEnums_1.SuccessStatus.OK).json({
                    error: null,
                    msg: HttpEnums_1.SuccessMsg.OK,
                    result,
                });
            }
            catch (error) {
                next(error);
            }
        });
    }
    delete(req, res, next) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                let { id: user_id } = req.user;
                let post_id = Number(req.params.id);
                let result = yield post_service_1.default.delete(post_id, user_id, next);
                if (!result) {
                    res.status(HttpEnums_1.ErrorStatus.NOT_FOUND).json({
                        error: true,
                        msg: HttpEnums_1.ErrorMsg.NOT_FOUND,
                    });
                    return;
                }
                res.status(HttpEnums_1.SuccessStatus.OK).json({
                    error: null,
                    msg: HttpEnums_1.SuccessMsg.OK,
                    result,
                });
            }
            catch (error) {
                next(error);
            }
        });
    }
    ratePost(req, res, next) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                let { id: user_id } = req.user;
                let post_id = Number(req.params.id);
                let { value } = req.body;
                const { errors } = (0, express_validator_1.validationResult)(req);
                if (errors.length) {
                    return res.status(HttpEnums_1.ErrorStatus.BAD_REQUEST).json({
                        error: true,
                        msg: errors[0].msg,
                    });
                }
                let result = yield post_service_1.default.ratePost(post_id, user_id, value, next);
                if (!result) {
                    res.status(HttpEnums_1.ErrorStatus.NOT_FOUND).json({
                        error: true,
                        msg: HttpEnums_1.ErrorMsg.NOT_FOUND,
                    });
                    return;
                }
                res.status(HttpEnums_1.SuccessStatus.OK).json({
                    error: null,
                    msg: HttpEnums_1.SuccessMsg.OK,
                    result,
                });
            }
            catch (error) {
                next(error);
            }
        });
    }
}
exports.default = new PostController();
//# sourceMappingURL=post.controller.js.map