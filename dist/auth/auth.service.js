"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const Auth_model_1 = require("./entities/Auth.model");
const User_model_1 = require("../user/entities/User.model");
class AuthService {
    signUp(email, password, next) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                let result = yield User_model_1.User.create({
                    email,
                    password,
                });
                return result;
            }
            catch (error) {
                next(error);
            }
        });
    }
    saveRefresh(user_id, token, next) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                let result = yield Auth_model_1.AuthSession.create({
                    user_id,
                    token,
                });
                return result;
            }
            catch (error) {
                next(error);
            }
        });
    }
    logout(user_id, token, next) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                let result = yield Auth_model_1.AuthSession.destroy({
                    where: {
                        user_id,
                        token,
                    },
                });
                if (result) {
                    return true;
                }
                else {
                    return false;
                }
            }
            catch (error) {
                next(error);
            }
        });
    }
    findRefresh(user_id, token, next) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                let result = yield Auth_model_1.AuthSession.findAll({
                    where: {
                        user_id,
                        token,
                    },
                });
                return result;
            }
            catch (error) {
                next(error);
            }
        });
    }
}
exports.default = new AuthService();
//# sourceMappingURL=auth.service.js.map