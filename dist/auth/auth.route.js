"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = require("express");
const HttpEnums_1 = require("../helpers/HttpEnums");
const express_validator_1 = require("express-validator");
const auth_access_middleware_1 = require("../middlewares/auth-access.middleware");
const auth_controller_1 = __importDefault(require("./auth.controller"));
const router = (0, express_1.Router)();
router.post("/signin", (0, express_validator_1.check)("email").isEmail().withMessage(HttpEnums_1.ErrorMsg.INVALID_EMAIL).bail(), (0, express_validator_1.check)("password")
    .isLength({ min: 6 })
    .withMessage(HttpEnums_1.ErrorMsg.INVALID_PASSWORD)
    .bail(), auth_controller_1.default.signIn);
router.post("/signup", (0, express_validator_1.check)("email").isEmail().withMessage(HttpEnums_1.ErrorMsg.INVALID_EMAIL).bail(), (0, express_validator_1.check)("password")
    .isLength({ min: 6 })
    .withMessage(HttpEnums_1.ErrorMsg.INVALID_PASSWORD)
    .bail(), auth_controller_1.default.signUp);
router.post("/refresh", auth_access_middleware_1.authAccessUser, auth_controller_1.default.refresh);
router.post("/logout", auth_access_middleware_1.authAccessUser, auth_controller_1.default.logout);
exports.default = router;
//# sourceMappingURL=auth.route.js.map