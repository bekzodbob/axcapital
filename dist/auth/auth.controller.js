"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_validator_1 = require("express-validator");
const bcrypt_1 = __importDefault(require("bcrypt"));
const auth_service_1 = __importDefault(require("./auth.service"));
const HttpEnums_1 = require("../helpers/HttpEnums");
const constants_1 = require("../helpers/constants");
const jwt_1 = require("../helpers/jwt");
const user_service_1 = __importDefault(require("../user/user.service"));
class AuthController {
    signUp(req, res, next) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                let { email, password } = req.body;
                const { errors } = (0, express_validator_1.validationResult)(req);
                if (errors.length) {
                    return res.status(HttpEnums_1.ErrorStatus.BAD_REQUEST).json({
                        error: true,
                        msg: errors[0].msg,
                    });
                }
                let found_user = yield user_service_1.default.findOne(email, next);
                if (found_user) {
                    res.status(HttpEnums_1.ErrorStatus.WRONG_CREDENTIALS).json({
                        error: true,
                        msg: HttpEnums_1.ErrorMsg.WRONG_CREDENTIALS,
                    });
                    return;
                }
                password = bcrypt_1.default.hashSync(password, 7);
                let result = yield auth_service_1.default.signUp(email, password, next);
                const jwt_user = {
                    id: result.id,
                    email: result.email,
                    created_at: result.created_at,
                    updated_at: result.updated_at,
                };
                delete result.password;
                let { access_token, refresh_token } = (0, jwt_1.signJwt)(jwt_user);
                res.clearCookie("refresh");
                yield auth_service_1.default.saveRefresh(result.id, refresh_token, next);
                res.cookie("refresh", refresh_token, {
                    maxAge: (0, constants_1.REFRESH_TOKEN_AGE)(),
                    // httpOnly: true,
                    // secure: true,
                });
                res.status(HttpEnums_1.SuccessStatus.CREATED).json({
                    error: null,
                    msg: HttpEnums_1.SuccessMsg.CREATED,
                    result: {
                        user: result,
                        access_token,
                    },
                });
            }
            catch (error) {
                next(error);
            }
        });
    }
    signIn(req, res, next) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                let { email, password } = req.body;
                const { errors } = (0, express_validator_1.validationResult)(req);
                if (errors.length) {
                    return res.status(HttpEnums_1.ErrorStatus.BAD_REQUEST).json({
                        error: true,
                        msg: errors[0].msg,
                    });
                }
                let found_user = yield user_service_1.default.findOne(email, next);
                if (!found_user || !bcrypt_1.default.compareSync(password, found_user.password)) {
                    res.status(HttpEnums_1.ErrorStatus.WRONG_CREDENTIALS).json({
                        error: true,
                        msg: HttpEnums_1.ErrorMsg.WRONG_CREDENTIALS,
                    });
                    return;
                }
                const jwt_user = {
                    id: found_user.id,
                    email: found_user.email,
                    created_at: found_user.created_at,
                    updated_at: found_user.updated_at,
                };
                delete found_user.password;
                let { access_token, refresh_token } = (0, jwt_1.signJwt)(jwt_user);
                res.clearCookie("refresh");
                yield auth_service_1.default.saveRefresh(found_user.id, refresh_token, next);
                res.cookie("refresh", refresh_token, {
                    maxAge: (0, constants_1.REFRESH_TOKEN_AGE)(),
                    // httpOnly: true,
                    // secure: true,
                });
                res.status(HttpEnums_1.SuccessStatus.OK).json({
                    error: null,
                    msg: HttpEnums_1.SuccessMsg.OK,
                    result: {
                        user: found_user,
                        access_token,
                    },
                });
            }
            catch (error) {
                next(error);
            }
        });
    }
    refresh(req, res, next) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                let { user } = req;
                let tokens = (0, jwt_1.signJwt)(user);
                let access_token = tokens === null || tokens === void 0 ? void 0 : tokens.access_token;
                res.status(HttpEnums_1.SuccessStatus.OK).json({
                    error: null,
                    msg: HttpEnums_1.SuccessMsg.OK,
                    user,
                    access_token,
                });
            }
            catch (error) {
                next(error);
            }
        });
    }
    logout(req, res, next) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                let { user } = req;
                let refresh_cookie = req.headers.cookie;
                let refresh_token = refresh_cookie.split("=")[1];
                yield auth_service_1.default.logout(user.id, refresh_token, next);
                res.clearCookie("refresh");
                res.status(HttpEnums_1.SuccessStatus.OK).json({
                    error: null,
                    msg: HttpEnums_1.SuccessMsg.OK,
                });
            }
            catch (error) {
                next(error);
            }
        });
    }
}
exports.default = new AuthController();
//# sourceMappingURL=auth.controller.js.map