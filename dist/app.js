"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = __importDefault(require("express"));
const dotenv_1 = __importDefault(require("dotenv"));
const express_rate_limit_1 = __importDefault(require("express-rate-limit"));
const helmet_1 = __importDefault(require("helmet"));
const cors_1 = __importDefault(require("cors"));
const cookie_parser_1 = __importDefault(require("cookie-parser"));
require("reflect-metadata");
const index_route_1 = __importDefault(require("./helpers/index.route"));
const not_found_handler_1 = require("./helpers/not-found.handler");
const error_handler_1 = require("./helpers/error.handler");
const db_1 = require("./db");
const app = (0, express_1.default)();
const limiter = (0, express_rate_limit_1.default)({
    windowMs: 1 * 60 * 1000,
    max: 40,
    standardHeaders: true,
    legacyHeaders: false, // Disable the `X-RateLimit-*` headers
});
app.use(express_1.default.json());
app.use((0, cookie_parser_1.default)());
app.use((0, cors_1.default)());
app.use((0, helmet_1.default)());
app.use(limiter);
// app.use(
//   morgan(
//     ":method :host :status :param[id] :res[content-length] - :response-time ms"
//   )
// );
dotenv_1.default.config();
const port = process.env.PORT;
app.use("/api", index_route_1.default);
app.use("/*", not_found_handler_1.notFoundHandler);
app.use(error_handler_1.errorHandler);
db_1.sequelize.sync().then(() => {
    console.log("DB:INIT");
});
app.listen(port, () => {
    console.log(`[server]: Server is running at http://localhost:${port}`);
});
//# sourceMappingURL=app.js.map