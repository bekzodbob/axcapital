"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.sequelize = void 0;
const sequelize_typescript_1 = require("sequelize-typescript");
const User_model_1 = require("./user/entities/User.model");
const Post_model_1 = require("./post/entities/Post.model");
const Auth_model_1 = require("./auth/entities/Auth.model");
const Post_View_model_1 = require("./post/entities/Post-View.model");
const Post_Rating_1 = require("./post/entities/Post-Rating");
exports.sequelize = new sequelize_typescript_1.Sequelize({
    database: "",
    dialect: "sqlite",
    username: "root",
    password: "",
    storage: "./root.sqlite",
    models: [User_model_1.User, Post_model_1.Post, Auth_model_1.AuthSession, Post_View_model_1.PostViews, Post_Rating_1.PostRating],
});
//# sourceMappingURL=db.js.map