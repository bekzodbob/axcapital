import { NextFunction } from "express";
import { User } from "../user/entities/User.model";
import { IPostView } from "../interfaces/post-view.interface";
import { IPost } from "../interfaces/post.interface";
import { Post } from "./entities/Post.model";
import Sequelize from "sequelize";
import { PostViews } from "./entities/Post-View.model";
import { PostRating } from "./entities/Post-Rating";
import calculateAverage from "../helpers/calculate-average";
import { IPostRating } from "../interfaces/post-rating.interface";
import readTime from "../helpers/calculate-read-time";

class PostService {
  async create(
    user_id: number,
    title: string,
    content: string,
    next: NextFunction
  ): Promise<IPost> {
    try {
      let result = await Post.create({
        author: user_id,
        title,
        content,
      });

      return result;
    } catch (error) {
      next(error);
    }
  }

  async getAll(offset: number, next: NextFunction): Promise<IPost[]> {
    try {
      let result: IPost[];
      if (offset) {
        result = await Post.findAll({
          where: {
            id: {
              [Sequelize.Op.lt]: offset,
            },
          },
          order: [["id", "DESC"]],
          limit: 10,
          include: [
            {
              model: User,
              attributes: ["id", "email", "created_at"],
            },
          ],
        });
      } else {
        result = await Post.findAll({
          order: [["id", "DESC"]],
          limit: 10,
          include: [
            {
              model: User,
              attributes: ["id", "email", "created_at"],
            },
          ],
        });
      }

      return result;
    } catch (error) {
      next(error);
    }
  }

  async getUserPosts(
    user_id: number,
    offset: number,
    next: NextFunction
  ): Promise<IPost[]> {
    try {
      let result: IPost[];
      if (offset) {
        result = await Post.findAll({
          where: {
            id: {
              [Sequelize.Op.gt]: offset,
            },
            author: user_id,
          },
          order: [["id", "DESC"]],
          limit: 10,
        });
      } else {
        result = await Post.findAll({
          where: {
            author: user_id,
          },
          order: [["id", "DESC"]],
          limit: 10,
        });
      }

      return result;
    } catch (error) {
      next(error);
    }
  }

  async getOne(id: number, next: NextFunction): Promise<IPost> {
    try {
      let result: IPost = await Post.findOne({
        where: {
          id,
        },
        include: [
          {
            model: User,
            attributes: ["id", "email", "created_at"],
          },
        ],
      });
      let {
        title,
        content,
        rating,
        author,
        created_at,
        updated_at,
        author_info,
      } = result;
      let read_time = readTime(content);
      return {
        id,
        title,
        content,
        rating,
        author,
        created_at,
        updated_at,
        author_info,
        read_time,
      };
    } catch (error) {
      next(error);
    }
  }

  async update(
    post_id: number,
    user_id: number,
    title: string,
    content: string,
    next: NextFunction
  ): Promise<IPost | []> {
    try {
      let updated = await Post.update(
        {
          title,
          content,
        },
        {
          where: {
            id: post_id,
            author: user_id,
          },
        }
      );
      if (!updated) {
        return [];
      }
      let result = await Post.findOne({
        where: {
          id: post_id,
          author: user_id,
        },
        include: [
          {
            model: User,
            attributes: ["id", "email", "created_at"],
          },
        ],
      });
      return result;
    } catch (error) {
      next(error);
    }
  }

  async delete(post_id: number, user_id: number, next: NextFunction) {
    try {
      let result = await Post.destroy({
        where: {
          id: post_id,
          author: user_id,
        },
      });

      return result;
    } catch (error) {
      next(error);
    }
  }

  async findOne(id: number, next: NextFunction): Promise<IPost> {
    try {
      let result: IPost = await Post.findOne({
        where: {
          id,
        },
      });

      return result;
    } catch (error) {
      next(error);
    }
  }

  async postView(
    post_id: number,
    user_id: number | null,
    next: NextFunction
  ): Promise<IPostView> {
    try {
      let result = await PostViews.create(
        {
          post_id,
          user_id,
        },
        {
          returning: true,
        }
      );
      return result;
    } catch (error) {
      next(error);
    }
  }

  async ratePost(
    post_id: number,
    user_id: number | null,
    value: number,
    next: NextFunction
  ): Promise<IPostRating> {
    try {
      let result: IPostRating;
      let rated = await PostRating.findOne({
        where: {
          post_id,
          user_id,
        },
      });
      if (!rated) {
        result = await PostRating.create(
          {
            post_id,
            user_id,
            value,
          },
          {
            returning: true,
          }
        );
      } else {
        await PostRating.update(
          {
            value,
          },
          {
            where: {
              post_id,
              user_id,
            },
          }
        );
      }
      result = await PostRating.findOne({
        where: {
          post_id,
          user_id,
        },
      });
      let post: IPost = await this.findOne(post_id, next);
      let average_rating = await calculateAverage(post_id, next);

      await Post.update(
        {
          rating: average_rating,
        },
        {
          where: {
            id: post_id,
          },
        }
      );

      await User.update(
        {
          rating: average_rating,
        },
        {
          where: {
            id: post.author,
          },
        }
      );

      return result;
    } catch (error) {
      next(error);
    }
  }
}

export default new PostService();
