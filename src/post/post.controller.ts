import { Request, Response, NextFunction } from "express";
import PostService from "./post.service";
import {
  ErrorMsg,
  ErrorStatus,
  SuccessMsg,
  SuccessStatus,
} from "../helpers/HttpEnums";

import { CustomRequest } from "../interfaces/request.interface";
import { validationResult } from "express-validator";
import readTime from "../helpers/calculate-read-time";

class PostController {
  async create(req: CustomRequest, res: Response, next: NextFunction) {
    try {
      let { id } = req.user;
      let { title, content } = req.body;
      const { errors }: any = validationResult(req);

      if (errors.length) {
        return res.status(ErrorStatus.BAD_REQUEST).json({
          error: true,
          msg: errors[0].msg,
        });
      }
      let result = await PostService.create(id, title, content, next);
      res.status(SuccessStatus.CREATED).json({
        error: null,
        msg: SuccessMsg.CREATED,
        result,
      });
    } catch (error: any) {
      next(error);
    }
  }

  async getAll(req: Request, res: Response, next: NextFunction) {
    try {
      let last_id: number = Number(req.query.last_id) || 0;

      let result = await PostService.getAll(last_id, next);

      res.status(SuccessStatus.OK).json({
        error: null,
        msg: SuccessMsg.OK,
        result,
      });
    } catch (error: any) {
      next(error);
    }
  }

  async getOne(req: CustomRequest, res: Response, next: NextFunction) {
    try {
      let user_id = req.user.id || null;

      let id = Number(req.params.id);

      if (!id) {
        res.status(ErrorStatus.BAD_REQUEST).json({
          error: true,
          msg: ErrorMsg.BAD_REQUEST,
        });
        return;
      }
      let post = await PostService.getOne(id, next);

      if (!post) {
        res.status(ErrorStatus.NOT_FOUND).json({
          error: true,
          msg: ErrorMsg.NOT_FOUND,
          post,
        });
        return;
      }

      
      await PostService.postView(id, user_id, next);

      res.status(SuccessStatus.OK).json({
        error: null,
        msg: SuccessMsg.OK,
        post,
      });
    } catch (error: any) {
      next(error);
    }
  }

  async update(req: CustomRequest, res: Response, next: NextFunction) {
    try {
      let { id: user_id } = req.user;

      let post_id = Number(req.params.id);
      let { title, content } = req.body;

      const { errors }: any = validationResult(req);

      if (errors.length) {
        return res.status(ErrorStatus.BAD_REQUEST).json({
          error: true,
          msg: errors[0].msg,
        });
      }

      let result = await PostService.update(
        post_id,
        user_id,
        title,
        content,
        next
      );

      if (!result) {
        res.status(ErrorStatus.NOT_FOUND).json({
          error: true,
          msg: ErrorMsg.NOT_FOUND,
        });
        return;
      }
      res.status(SuccessStatus.OK).json({
        error: null,
        msg: SuccessMsg.OK,
        result,
      });
    } catch (error: any) {
      next(error);
    }
  }

  async delete(req: CustomRequest, res: Response, next: NextFunction) {
    try {
      let { id: user_id } = req.user;

      let post_id = Number(req.params.id);

      let result = await PostService.delete(post_id, user_id, next);
      if (!result) {
        res.status(ErrorStatus.NOT_FOUND).json({
          error: true,
          msg: ErrorMsg.NOT_FOUND,
        });
        return;
      }

      res.status(SuccessStatus.OK).json({
        error: null,
        msg: SuccessMsg.OK,
        result,
      });
    } catch (error: any) {
      next(error);
    }
  }

  async ratePost(req: CustomRequest, res: Response, next: NextFunction) {
    try {
      let { id: user_id } = req.user;

      let post_id = Number(req.params.id);
      let { value } = req.body;

      const { errors }: any = validationResult(req);

      if (errors.length) {
        return res.status(ErrorStatus.BAD_REQUEST).json({
          error: true,
          msg: errors[0].msg,
        });
      }

      let result = await PostService.ratePost(post_id, user_id, value, next);

      if (!result) {
        res.status(ErrorStatus.NOT_FOUND).json({
          error: true,
          msg: ErrorMsg.NOT_FOUND,
        });
        return;
      }
      res.status(SuccessStatus.OK).json({
        error: null,
        msg: SuccessMsg.OK,
        result,
      });
    } catch (error: any) {
      next(error);
    }
  }
}

export default new PostController();
