import { Router } from "express";
import { check } from "express-validator";
import { ErrorMsg } from "../helpers/HttpEnums";
import { authUser } from "../middlewares/auth.middleware";
import { userInfo } from "../middlewares/user-info.middleware";
import PostController from "./post.controller";
const router = Router();

router.post(
  "/",
  check("title")
    .isLength({ min: 10, max: 150 })
    .withMessage(ErrorMsg.INVALID_TITLE)
    .bail(),
  check("content")
    .isLength({ min: 10, max: 25000 })
    .withMessage(ErrorMsg.INVALID_CONTENT)
    .bail(),
  authUser,
  PostController.create
);

router.get("/", PostController.getAll);

router.get("/:id", userInfo, PostController.getOne);

router.post(
  "/:id/rate",
  check("value")
    .isInt({ min: 1, max: 5 })
    .withMessage(ErrorMsg.INVALID_RATING)
    .bail(),
  authUser,
  PostController.ratePost
);

router.put(
  "/:id",
  check("title")
    .isLength({ min: 10, max: 150 })
    .withMessage(ErrorMsg.INVALID_TITLE)
    .bail(),
  check("content")
    .isLength({ min: 10, max: 25000 })
    .withMessage(ErrorMsg.INVALID_CONTENT)
    .bail(),
  authUser,
  PostController.update
);

router.delete("/:id", authUser, PostController.delete);

export default router;
