import {
  BelongsTo,
  Column,
  DataType,
  HasMany,
  Model,
  Table,
  CreatedAt,
  UpdatedAt,
  ForeignKey,
} from "sequelize-typescript";
import { IPostRating } from "../../interfaces/post-rating.interface";
import { IPost } from "../../interfaces/post.interface";
import { User } from "../../user/entities/User.model";
import { Post } from "./Post.model";
@Table({ tableName: "post_ratings" })
export class PostRating extends Model<IPostRating> {
  @Column({
    type: DataType.BIGINT,
    unique: true,
    autoIncrement: true,
    primaryKey: true,
  })
  id: bigint;

  @Column({
    type: DataType.SMALLINT,
    allowNull: false,
    validate: {
      min: 1,
      max: 5,
    },
  })
  value: number;

  @ForeignKey(() => User)
  @Column(DataType.INTEGER)
  user_id: number;

  @ForeignKey(() => Post)
  @Column(DataType.INTEGER)
  post_id: number;

  @CreatedAt
  @Column({ field: "created_at", type: DataType.DATE })
  created_at: Date;

  @UpdatedAt
  @Column({ field: "updated_at", type: DataType.DATE })
  updated_at: Date;
}
