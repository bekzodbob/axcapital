import {
  BelongsTo,
  Column,
  DataType,
  HasMany,
  Model,
  Table,
  CreatedAt,
  UpdatedAt,
  ForeignKey,
  AllowNull,
} from "sequelize-typescript";
import { IPost } from "../../interfaces/post.interface";
import { User } from "../../user/entities/User.model";
import { PostRating } from "./Post-Rating";
import { PostViews } from "./Post-View.model";
@Table({ tableName: "posts" })
export class Post extends Model<IPost> {
  @Column({
    type: DataType.INTEGER,
    unique: true,
    autoIncrement: true,
    primaryKey: true,
  })
  id: number;

  @Column({
    type: DataType.STRING(150),
    allowNull: false,
  })
  title: string;

  @Column({
    type: DataType.TEXT,
    allowNull: false,
  })
  content: string;

  @Column({
    type: DataType.DOUBLE,
  })
  rating: number;

  @ForeignKey(() => User)
  @Column({
    type: DataType.INTEGER,
    allowNull: false,
  })
  author: number;

  @BelongsTo(() => User)
  author_info: User;

  @HasMany(() => PostRating, {
    onDelete: "CASCADE",
    onUpdate: "CASCADE",
  })
  ratings: PostRating[];

  @CreatedAt
  @Column({ field: "created_at", type: DataType.DATE })
  created_at: Date;

  @UpdatedAt
  @Column({ field: "updated_at", type: DataType.DATE })
  updated_at: Date;
}
