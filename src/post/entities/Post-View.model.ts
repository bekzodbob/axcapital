import {
  BelongsToMany,
  Column,
  DataType,
  HasMany,
  Model,
  Table,
  CreatedAt,
  UpdatedAt,
  ForeignKey,
} from "sequelize-typescript";
import { IPostView } from "../../interfaces/post-view.interface";
import { User } from "../../user/entities/User.model";
import { Post } from "./Post.model";
@Table({ tableName: "post_views" })
export class PostViews extends Model<IPostView> {
  @Column({
    type: DataType.BIGINT,
    unique: true,
    autoIncrement: true,
    primaryKey: true,
  })
  id: bigint;

  @ForeignKey(() => User)
  @Column(DataType.INTEGER)
  user_id: number;

  @ForeignKey(() => Post)
  @Column(DataType.INTEGER)
  post_id: number;

  @CreatedAt
  @Column({ field: "created_at", type: DataType.DATE })
  created_at: Date;
}
