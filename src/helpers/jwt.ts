import jwt, { JwtPayload } from "jsonwebtoken";
import { IUser } from "../interfaces/user.interface";
import { ACCESS_TOKEN_AGE, REFRESH_TOKEN_AGE } from "./constants";
import { err } from "./logger.service";
const SECRET_KEY = String(process.env.JWT_SERVER_KEY);

function signJwt(user: IUser) {
  try {

    let access_token = jwt.sign(
      {
        exp: ACCESS_TOKEN_AGE(),
        data: user,
      },
      SECRET_KEY
    );

    let refresh_token = jwt.sign(
      {
        exp: REFRESH_TOKEN_AGE(),
        data: user,
      },
      SECRET_KEY
    );

    return { refresh_token, access_token };
  } catch (error: any) {
    err(error);
  }
}

function decodeJwt(token: string): JwtPayload | undefined {
  try {
    let decoded = jwt.verify(token, SECRET_KEY) as JwtPayload;

    return decoded;
  } catch (error: any) {
    err(error);
  }
}

export { signJwt, decodeJwt };
