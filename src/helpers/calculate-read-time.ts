export default function readTime(content: string): number {
  return Math.round(content.trim().split(/\s+/).length / 238);
}
