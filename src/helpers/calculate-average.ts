import { NextFunction } from "express";
import { Sequelize } from "sequelize";
import { PostRating } from "../post/entities/Post-Rating";

export default async function calculateAverage(
  post_id: number,
  next: NextFunction
): Promise<number> {
  try {
    const result = await PostRating.findOne({
      attributes: [
        [Sequelize.fn("AVG", Sequelize.col("value")), "averageRating"],
      ],
      where: { post_id },
    });
    let average = result.get("averageRating") as number;
    return average;
  } catch (error) {
    next(error);
  }
}
