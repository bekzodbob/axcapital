import { Request, Response, NextFunction } from "express";
import { ErrorMsg, ErrorStatus } from "./HttpEnums";

export function notFoundHandler(
  req: Request,
  res: Response,
  next: NextFunction
) {
  res.status(ErrorStatus.NOT_FOUND).json({
    error: true,
    msg: ErrorMsg.NOT_FOUND,
  });
  return;
}
