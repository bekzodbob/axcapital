const ACCESS_TOKEN_AGE = (): number => {
  return Math.floor(Date.now() / 1000) + 60 * 5;
};

const REFRESH_TOKEN_AGE = (): number => {
  return Math.floor(Date.now() / 1000) + 60 * 60 * 24 * 30;
};

export { ACCESS_TOKEN_AGE, REFRESH_TOKEN_AGE };
