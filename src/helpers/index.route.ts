import { Router } from "express";
import authRoute from "../auth/auth.route";
import postRoute from "../post/post.route";
import userRoute from "../user/user.route";
const router = Router();

router.use("/auth", authRoute);

router.use("/post", postRoute);

router.use("/user", userRoute);

export default router;
