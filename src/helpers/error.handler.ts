import { Request, Response, NextFunction } from "express";
import { ErrorMsg, ErrorStatus } from "./HttpEnums";
import { err } from "./logger.service";

export function errorHandler(
  error: any,
  req: Request,
  res: Response,
  next: NextFunction
) {
  err(error);
  res.status(ErrorStatus.SERVER_ERROR).json({
    error: true,
    msg: ErrorMsg.SERVER_ERROR,
  });
  return;
}
