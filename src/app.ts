import express from "express";
import dotenv from "dotenv";

import rateLimit from "express-rate-limit";
import helmet from "helmet";
import cors from "cors";
import cookieParser from "cookie-parser";
import "reflect-metadata";
import indexRoute from "./helpers/index.route";
import { IUser } from "./interfaces/user.interface";
import { notFoundHandler } from "./helpers/not-found.handler";
import { errorHandler } from "./helpers/error.handler";
import { sequelize } from "./db";
const app = express();

declare global {
  namespace Express {
    interface Request {
      user: IUser;
    }
  }
}

const limiter = rateLimit({
  windowMs: 1 * 60 * 1000, // 1 minutes
  max: 40, // Limit each IP to 40 requests per `window` (here, per 15 minutes)
  standardHeaders: true, // Return rate limit info in the `RateLimit-*` headers
  legacyHeaders: false, // Disable the `X-RateLimit-*` headers
});
app.use(express.json());
app.use(cookieParser());
app.use(cors());
app.use(helmet());
app.use(limiter);
// app.use(
//   morgan(
//     ":method :host :status :param[id] :res[content-length] - :response-time ms"
//   )
// );
dotenv.config();

const port = process.env.PORT;

app.use("/api", indexRoute);

app.use("/*", notFoundHandler);

app.use(errorHandler);

sequelize.sync().then(() => {
  console.log("DB:INIT");
});

app.listen(port, () => {
  console.log(`[server]: Server is running at http://localhost:${port}`);
});
