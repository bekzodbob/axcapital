import {
  BelongsToMany,
  Column,
  DataType,
  HasMany,
  Model,
  Table,
  ForeignKey,
  CreatedAt,
  BelongsTo,
} from "sequelize-typescript";
import { User } from "../../user/entities/User.model";
import { IRefreshToken } from "../../interfaces/refresh-token.interface";

@Table({ tableName: "user_tokens" })
export class AuthSession extends Model<IRefreshToken> {
  @Column({
    type: DataType.INTEGER,
    unique: true,
    autoIncrement: true,
    primaryKey: true,
  })
  id: bigint;

  @Column({
    type: DataType.STRING(260),
    allowNull: false,
  })
  token: string;

  @ForeignKey(() => User)
  @Column(DataType.INTEGER)
  user_id: number;

  @BelongsTo(() => User)
  userId: User;

  @CreatedAt
  @Column({ field: "created_at", type: DataType.DATE })
  created_at: Date;
}
