import { NextFunction } from "express";
import { IRefreshToken } from "../interfaces/refresh-token.interface";
import { IUser } from "../interfaces/user.interface";
import { AuthSession } from "./entities/Auth.model";
import { User } from "../user/entities/User.model";
class AuthService {
  async signUp(
    email: string,
    password: string,
    next: NextFunction
  ): Promise<IUser> {
    try {
      let result = await User.create({
        email,
        password,
      });

      return result;
    } catch (error: any) {
      next(error);
    }
  }

  async saveRefresh(
    user_id: number,
    token: string,
    next: NextFunction
  ): Promise<IRefreshToken> {
    try {
      let result = await AuthSession.create({
        user_id,
        token,
      });
      return result;
    } catch (error: any) {
      next(error);
    }
  }

  async logout(
    user_id: number | undefined,
    token: string,
    next: NextFunction
  ): Promise<boolean> {
    try {
      let result: number = await AuthSession.destroy({
        where: {
          user_id,
          token,
        },
      });
      if (result) {
        return true;
      } else {
        return false;
      }
    } catch (error: any) {
      next(error);
    }
  }

  async findRefresh(
    user_id: number | undefined,
    token: string,
    next: NextFunction
  ): Promise<IRefreshToken[]> {
    try {
      let result = await AuthSession.findAll({
        where: {
          user_id,
          token,
        },
      });
      return result;
    } catch (error: any) {
      next(error);
    }
  }
}

export default new AuthService();
