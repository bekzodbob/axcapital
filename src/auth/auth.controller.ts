import { Request, Response, NextFunction } from "express";
import { validationResult, ErrorFormatter } from "express-validator";
import bcrypt from "bcrypt";
import AuthService from "./auth.service";
import {
  ErrorMsg,
  ErrorStatus,
  SuccessMsg,
  SuccessStatus,
} from "../helpers/HttpEnums";
import { ACCESS_TOKEN_AGE, REFRESH_TOKEN_AGE } from "../helpers/constants";
import { IUser } from "../interfaces/user.interface";
import { signJwt } from "../helpers/jwt";
import { CustomRequest } from "../interfaces/request.interface";
import UserService from "../user/user.service";

class AuthController {
  async signUp(req: Request, res: Response, next: NextFunction) {
    try {
      let { email, password } = req.body;
      const { errors }: any = validationResult(req);

      if (errors.length) {
        return res.status(ErrorStatus.BAD_REQUEST).json({
          error: true,
          msg: errors[0].msg,
        });
      }

      let found_user: IUser = await UserService.findOne(email, next);

      if (found_user) {
        res.status(ErrorStatus.WRONG_CREDENTIALS).json({
          error: true,
          msg: ErrorMsg.WRONG_CREDENTIALS,
        });
        return;
      }

      password = bcrypt.hashSync(password, 7);

      let result = await AuthService.signUp(email, password, next);

      const jwt_user = {
        id: result.id,
        email: result.email,
        created_at: result.created_at,
        updated_at: result.updated_at,
      };
      delete result.password;

      let { access_token, refresh_token } = signJwt(jwt_user);
      res.clearCookie("refresh");

      await AuthService.saveRefresh(result.id, refresh_token, next);

      res.cookie("refresh", refresh_token, {
        maxAge: REFRESH_TOKEN_AGE(),
        // httpOnly: true,
        // secure: true,
      });
      res.status(SuccessStatus.CREATED).json({
        error: null,
        msg: SuccessMsg.CREATED,
        result: {
          user: result,
          access_token,
        },
      });
    } catch (error: any) {
      next(error);
    }
  }

  async signIn(req: Request, res: Response, next: NextFunction) {
    try {
      let { email, password } = req.body;
      const { errors }: any = validationResult(req);

      if (errors.length) {
        return res.status(ErrorStatus.BAD_REQUEST).json({
          error: true,
          msg: errors[0].msg,
        });
      }

      let found_user: IUser = await UserService.findOne(email, next);

      if (!found_user || !bcrypt.compareSync(password, found_user.password)) {
        res.status(ErrorStatus.WRONG_CREDENTIALS).json({
          error: true,
          msg: ErrorMsg.WRONG_CREDENTIALS,
        });
        return;
      }

      const jwt_user = {
        id: found_user.id,
        email: found_user.email,
        created_at: found_user.created_at,
        updated_at: found_user.updated_at,
      };
      delete found_user.password;
      let { access_token, refresh_token } = signJwt(jwt_user);
      res.clearCookie("refresh");

      await AuthService.saveRefresh(found_user.id, refresh_token, next);

      res.cookie("refresh", refresh_token, {
        maxAge: REFRESH_TOKEN_AGE(),
        // httpOnly: true,
        // secure: true,
      });
      res.status(SuccessStatus.OK).json({
        error: null,
        msg: SuccessMsg.OK,
        result: {
          user: found_user,
          access_token,
        },
      });
    } catch (error: any) {
      next(error);
    }
  }

  async refresh(req: CustomRequest, res: Response, next: NextFunction) {
    try {
      let { user } = req;
      let tokens = signJwt(user);
      let access_token = tokens?.access_token;
      res.status(SuccessStatus.OK).json({
        error: null,
        msg: SuccessMsg.OK,
        user,
        access_token,
      });
    } catch (error: any) {
      next(error);
    }
  }

  async logout(req: Request, res: Response, next: NextFunction) {
    try {
      let { user } = req;
      let refresh_cookie = req.headers.cookie as string;
      let refresh_token = refresh_cookie.split("=")[1];
      await AuthService.logout(user.id, refresh_token, next);
      res.clearCookie("refresh");
      res.status(SuccessStatus.OK).json({
        error: null,
        msg: SuccessMsg.OK,
      });
    } catch (error: any) {
      next(error);
    }
  }
}

export default new AuthController();
