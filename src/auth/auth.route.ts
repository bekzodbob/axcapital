import { Router } from "express";
import { ErrorMsg } from "../helpers/HttpEnums";
import { check } from "express-validator";
import { authAccessUser } from "../middlewares/auth-access.middleware";
import AuthController from "./auth.controller";
const router = Router();

router.post(
  "/signin",
  check("email").isEmail().withMessage(ErrorMsg.INVALID_EMAIL).bail(),
  check("password")
    .isLength({ min: 6 })
    .withMessage(ErrorMsg.INVALID_PASSWORD)
    .bail(),

  AuthController.signIn
);

router.post(
  "/signup",
  check("email").isEmail().withMessage(ErrorMsg.INVALID_EMAIL).bail(),
  check("password")
    .isLength({ min: 6 })
    .withMessage(ErrorMsg.INVALID_PASSWORD)
    .bail(),

  AuthController.signUp
);

router.post("/refresh", authAccessUser, AuthController.refresh);

router.post("/logout", authAccessUser, AuthController.logout);

export default router;
