import { NextFunction } from "express";
import { Post } from "../post/entities/Post.model";
import { IUser } from "../interfaces/user.interface";
import { IPost } from "../interfaces/post.interface";
import { User } from "./entities/User.model";
import Sequelize from "sequelize";

class UserService {
  async getAllUsers(offset: number, next: NextFunction): Promise<IUser[]> {
    try {
      if (offset) {
        let result: IUser[] = await User.findAll({
          where: {
            id: {
              [Sequelize.Op.gt]: offset,
            },
          },
          order: [["id", "ASC"]],
          limit: 10,
          attributes: ["id", "email", "created_at"],
        });
        return result;
      } else {
        let result: IUser[] = await User.findAll({
          order: [["id", "ASC"]],
          limit: 10,
          attributes: ["id", "email", "created_at"],
        });
        return result;
      }
    } catch (error: any) {
      next(error);
    }
  }

  async getUser(id: number, next: NextFunction): Promise<IUser> {
    try {
      let result: IUser = await User.findOne({
        where: {
          id,
        },
        attributes: ["id", "email", "created_at"],
      });
      return result;
    } catch (error: any) {
      next(error);
    }
  }

  async findOne(email: string, next: NextFunction): Promise<IUser> {
    try {
      let result: IUser = await User.findOne({
        where: {
          email,
        },
        include: [{ all: true }],
      });
      return result;
    } catch (error: any) {
      next(error);
    }
  }
}
export default new UserService();
