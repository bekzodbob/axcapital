import { NextFunction, Response, Request } from "express";
import PostService from "../post/post.service";
import { SuccessMsg, SuccessStatus } from "../helpers/HttpEnums";
import { CustomRequest } from "../interfaces/request.interface";
import UserService from "./user.service";

class UserController {
  async getAllUser(req: Request, res: Response, next: NextFunction) {
    try {
      let offset = Number(req.query.last_id) || 0;

      let result = await UserService.getAllUsers(offset, next);

      res.status(SuccessStatus.OK).json({
        error: null,
        msg: SuccessMsg.OK,
        result,
      });
    } catch (error) {
      next(error);
    }
  }

  async getUser(req: Request, res: Response, next: NextFunction) {
    try {
      let user_id = Number(req.params.user_id);
      let result = await UserService.getUser(user_id, next);

      res.status(SuccessStatus.OK).json({
        error: null,
        msg: SuccessMsg.OK,
        result,
      });
    } catch (error) {
      next(error);
    }
  }

  async getUserPosts(req: Request, res: Response, next: NextFunction) {
    try {
      let user_id = Number(req.params.user_id);
      let last_id = Number(req.query.last_id) || 0;
      let result = await PostService.getUserPosts(user_id, last_id, next);

      res.status(SuccessStatus.OK).json({
        error: null,
        msg: SuccessMsg.OK,
        result,
      });
    } catch (error) {
      next(error);
    }
  }
}

export default new UserController();
