import {
  BelongsToMany,
  Column,
  DataType,
  HasMany,
  Model,
  Table,
  CreatedAt,
  UpdatedAt,
} from "sequelize-typescript";
import { Post } from "../../post/entities/Post.model";
import { IUser } from "../../interfaces/user.interface";

@Table({ tableName: "users" })
export class User extends Model<IUser> {
  @Column({
    type: DataType.INTEGER,
    unique: true,
    autoIncrement: true,
    primaryKey: true,
  })
  id: number;

  @Column({
    type: DataType.STRING(255),
    unique: true,
    allowNull: false,
  })
  email: string;

  @Column({
    type: DataType.STRING(60),
    allowNull: false,
  })
  password: string;

  @Column({
    type: DataType.DOUBLE,
  })
  rating: number;

  @HasMany(() => Post, {
    onUpdate: "CASCADE",
    onDelete: "CASCADE",
  })
  posts: Post[];

  @CreatedAt
  @Column({ field: "created_at", type: DataType.DATE })
  created_at: Date;

  @UpdatedAt
  @Column({ field: "updated_at", type: DataType.DATE })
  updated_at: Date;
}
