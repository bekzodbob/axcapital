import { Router } from "express";
import { authAccessUser } from "../middlewares/auth-access.middleware";
import { authUser } from "../middlewares/auth.middleware";
import UserController from "./user.controller";
const router = Router();

router.get("/", UserController.getAllUser);

router.get("/:user_id", UserController.getUser);

router.get("/:user_id/post", UserController.getUserPosts);

export default router;
