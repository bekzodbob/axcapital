import { Sequelize } from "sequelize-typescript";
import { User } from "./user/entities/User.model";
import { Post } from "./post/entities/Post.model";
import { AuthSession } from "./auth/entities/Auth.model";
import { PostViews } from "./post/entities/Post-View.model";
import { PostRating } from "./post/entities/Post-Rating";
export const sequelize = new Sequelize({
  database: "",
  dialect: "sqlite",
  username: "root",
  password: "",
  storage: "./root.sqlite",
  models: [User, Post, AuthSession, PostViews, PostRating],
});
