import { IPost } from "./post.interface";

interface IUser {
  id: number;
  email: string;
  password?: string;
  rating?: number | null;
  updated_at: Date;
  created_at: Date;
  posts?: IPost[];
}

export { IUser };
