export interface IPostRating {
  id: bigint;
  user_id: number;
  post_id: number;
  value: number;
  updated_at: Date;
  created_at: Date;
}
