import { IUser } from "./user.interface";

export interface IPost {
  id: number;
  title: string;
  content: string;
  author: number;
  author_info?: IUser;
  rating?: number | null;
  read_time?: number;
  updated_at: Date;
  created_at: Date;
}
