export interface IComment {
  id: number;
  body: string;
  creator: number;
  post_id: number;
  created_at: string;
  updated_at: string;
}
