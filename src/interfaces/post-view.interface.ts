export interface IPostView {
  id: bigint;
  user_id?: number;
  post_id: number;
  created_at: Date;
}
