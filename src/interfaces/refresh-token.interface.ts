export interface IRefreshToken {
  id: bigint;
  user_id: number;
  token: string;
  created_at: Date;
}
