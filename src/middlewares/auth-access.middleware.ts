import { Request, Response, NextFunction } from "express";
import { JwtPayload } from "jsonwebtoken";
import { ErrorMsg, ErrorStatus } from "../helpers/HttpEnums";
import { decodeJwt } from "../helpers/jwt";
import { IUser } from "../interfaces/user.interface";
import { CustomRequest } from "../interfaces/request.interface";
import AuthModel from "../auth/auth.service";

async function authAccessUser(
  req: CustomRequest,
  res: Response,
  next: NextFunction
) {
  let refresh_cookie = req.headers.cookie;

  if (!refresh_cookie) {
    res.status(ErrorStatus.NOT_AUTH).json({
      error: true,
      msg: ErrorMsg.NOT_AUTH,
    });
    return;
  }

  let refresh_token = refresh_cookie.split("=")[1];

  let decoded_jwt: JwtPayload | undefined = decodeJwt(refresh_token);
  if (!decoded_jwt) {
    res.status(ErrorStatus.NOT_AUTH).json({
      error: true,
      msg: ErrorMsg.NOT_AUTH,
    });
    return;
  }

  let refresh_active = await AuthModel.findRefresh(
    decoded_jwt.data.id,
    refresh_token,
    next
  );

  if (!refresh_active) {
    res.status(ErrorStatus.NOT_AUTH).json({
      error: true,
      msg: ErrorMsg.NOT_AUTH,
    });
    return;
  }
  req.user = decoded_jwt.data;
  next();
}

export { authAccessUser };
