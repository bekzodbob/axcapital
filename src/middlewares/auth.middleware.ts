import { Request, Response, NextFunction } from "express";
import { JwtPayload } from "jsonwebtoken";
import { ErrorMsg, ErrorStatus } from "../helpers/HttpEnums";
import { decodeJwt } from "../helpers/jwt";
import { IUser } from "../interfaces/user.interface";
import { CustomRequest } from "../interfaces/request.interface";

function authUser(req: CustomRequest, res: Response, next: NextFunction) {
  try {
    let access_token = req.headers["token"] as string;

    if (!access_token || !access_token.length) {
      res.status(ErrorStatus.NOT_AUTH).json({
        error: true,
        msg: ErrorMsg.NOT_AUTH,
      });
      return;
    }

    let decoded_jwt: JwtPayload | undefined = decodeJwt(access_token);
    if (!decoded_jwt || !decoded_jwt.data.id) {
      res.status(ErrorStatus.NOT_AUTH).json({
        error: true,
        msg: ErrorMsg.NOT_AUTH,
      });
      return;
    }

    req.user = decoded_jwt.data;

    next();
  } catch (error) {
    return next(error);
  }
}

export { authUser };
