import { Request, Response, NextFunction } from "express";
import { JwtPayload } from "jsonwebtoken";
import { ErrorMsg, ErrorStatus } from "../helpers/HttpEnums";
import { decodeJwt } from "../helpers/jwt";
import { CustomRequest } from "../interfaces/request.interface";

function userInfo(req: CustomRequest, res: Response, next: NextFunction) {
  try {
    let refresh_cookie = req.headers.cookie;

    if (!refresh_cookie) {
      return next();
    }

    let refresh_token = refresh_cookie.split("=")[1];
    let decoded_jwt: JwtPayload | undefined = decodeJwt(refresh_token);
    if (!decoded_jwt) {
      return next();
    }
    req.user = decoded_jwt.data;
    next();
  } catch (error) {
    return next(error);
  }
}

export { userInfo };
