import { Request, Response, NextFunction } from "express";
import { ErrorMsg, ErrorStatus } from "../helpers/HttpEnums";
import postModel from "../post/post.service";

async function postExists(req: Request, res: Response, next: NextFunction) {
  try {
    let post_id = Number(req.params.post_id);

    let does_exists = await postModel.findOne(post_id, next);

    if (!does_exists.id) {
      res.status(ErrorStatus.BAD_REQUEST).json({
        error: true,
        msg: ErrorMsg.BAD_REQUEST,
      });
      return;
    }
    next();
  } catch (error) {
    return next(error);
  }
}

export { postExists };
