CREATE TABLE users (
  id serial PRIMARY KEY,
  email VARCHAR (255) NOT NULL,
  password VARCHAR (60) NOT NULL,
  created_at timestamp without time zone DEFAULT now(),
  updated_at timestamp without time zone,
)

CREATE TABLE posts (
  id serial PRIMARY KEY,
  title VARCHAR (150) NOT NULL,
  content text NOT NULL,
  author int NOT NULL,
  created_at timestamp without time zone DEFAULT now(),
  updated_at timestamp without time zone,
  
  CONSTRAINT fk_user_post FOREIGN KEY(creator)
        REFERENCES users(id)
        ON DELETE CASCADE
);

CREATE TABLE post_views (
  id bigserial PRIMARY KEY,
  user_id int,
  post_id int NOT NULL,
  created_at timestamp without time zone DEFAULT now(),
  
  CONSTRAINT fk_post_view FOREIGN KEY(post_id)
        REFERENCES posts(id)
        ON DELETE CASCADE
)

CREATE TABLE user_tokens (
  id bigserial PRIMARY KEY,
  user_id integer NOT NULL,
  token VARCHAR(260),
  created_at timestamp without time zone DEFAULT now(),
  
  CONSTRAINT fk_user_token FOREIGN KEY(user_id)
        REFERENCES users(id)
        ON DELETE CASCADE
)